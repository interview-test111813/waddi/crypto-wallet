import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:waddicryptowallet/src/data/common/module/shared_pref_module.dart';
import 'package:waddicryptowallet/src/data/home/local/api/home_local_api_impl.dart';
import 'package:waddicryptowallet/src/data/home/remote/api/home_api_impl.dart';
import 'package:waddicryptowallet/src/domain/common/base/base_exception.dart';
import 'package:waddicryptowallet/src/utils/common.dart';
import 'consts.dart';
import 'home_remote_api_test.mocks.dart';

class MockSharedPreferenceModule extends Mock
    implements SharedPreferenceModule {}

@GenerateMocks([Dio])
void main() {
  group('Test Home Remote Api Implementation', () {
    late MockDio dio;
    late MockSharedPreferenceModule mockSharedPreferenceModule;
    late HomeLocalApiImpl homeLocalApi;
    late HomeApiImpl homeApi;

    setUp(() {
      dio = MockDio();
      mockSharedPreferenceModule = MockSharedPreferenceModule();
      homeLocalApi = HomeLocalApiImpl(mockSharedPreferenceModule);
      homeApi = HomeApiImpl(dio: dio, homeLocalApi: homeLocalApi);
    });

    test("Get Favorites assets List", () async {
      //define favorites
      final expectedAssets = ['bitcoin', 'cardano', 'ripple'];

      //set event return type
      when(mockSharedPreferenceModule.getAssetsFavorites())
          .thenReturn(expectedAssets);

      final List<String> favorites = homeLocalApi.getAssetsFavorites();
      expect(favorites, expectedAssets);

      expect(favorites.length, 3);
      expect("bitcoin", favorites[0]);
      expect("cardano", favorites[1]);
      expect("ripple", favorites[2]);

      verify(mockSharedPreferenceModule.getAssetsFavorites()).called(1);
    });

    test("should return a list of CoinModel when the request is successful",
        () async {
      final mockResponse = Response(
          data: apiAssetsJson,
          statusCode: 200,
          requestOptions: RequestOptions(path: ''));

      final expectedAssets = ['bitcoin'];

      //set event return type
      when(mockSharedPreferenceModule.getAssetsFavorites())
          .thenReturn(expectedAssets);

      when(dio.get('public/v1/coins?',
              queryParameters: {'skip': 0, 'limit': 100, 'currency': 'EUR'}))
          .thenAnswer((_) async => mockResponse);

      final assets = await homeApi.getCryptoAssets();

      expect(2, assets.length);
      expect("bitcoin", assets[0].id);
      expect(true, assets[0].fab);
      expect(false, assets[1].fab);
    });

    test('should throw a BaseException when the request fails', () async {
      // Arrange
      final response =
          Response(statusCode: 400, requestOptions: RequestOptions(path: ''));

      when(dio.get(any, queryParameters: anyNamed('queryParameters')))
          .thenAnswer((_) async => response);

      // Act
      final call = homeApi.getCryptoAssets;

      Logger.write("text");
      // Assert
      expect(call(), throwsA(isA<BaseException>()));
    });

    test('should throw a BaseException when a DioError occurs', () async {
      // Arrange
      when(dio.get(any, queryParameters: anyNamed('queryParameters')))
          .thenThrow(DioError(requestOptions: RequestOptions(path: '')));

      // Act
      final call = homeApi.getCryptoAssets;

      // Assert
      expect(call(), throwsA(isA<BaseException>()));
    });

    test('should throw a BaseException when an Exception occurs', () async {
      // Arrange
      when(dio.get(any, queryParameters: anyNamed('queryParameters')))
          .thenThrow(Exception());

      // Act
      final call = homeApi.getCryptoAssets;

      // Assert
      expect(call(), throwsA(isA<BaseException>()));
    });
  });
}
