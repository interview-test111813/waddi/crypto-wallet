final apiAssetsJson = {
  "coins": [
    {
      "id": "bitcoin",
      "icon": "https://static.coinstats.app/coins/1650455588819.png",
      "name": "Bitcoin",
      "symbol": "BTC",
      "rank": 1,
      "price": 24903.604610280712,
      "priceBtc": 1,
      "volume": 62961731795.8393,
      "marketCap": 482498772484.2029,
      "availableSupply": 19374656,
      "totalSupply": 21000000,
      "priceChange1h": 0.31,
      "priceChange1d": -0.39,
      "priceChange1w": -2.02,
      "websiteUrl": "http://www.bitcoin.org",
      "twitterUrl": "https://twitter.com/bitcoin",
      "exp": [
        "https://blockchair.com/bitcoin/",
        "https://btc.com/",
        "https://btc.tokenview.io/"
      ]
    },
    {
      "id": "ethereum",
      "icon": "https://static.coinstats.app/coins/1650455629727.png",
      "name": "Ethereum",
      "symbol": "ETH",
      "rank": 2,
      "price": 1665.931560742348,
      "priceBtc": 0.06690427684214682,
      "volume": 29257581361.785507,
      "marketCap": 204839884392.60202,
      "availableSupply": 122958163,
      "totalSupply": 122958163,
      "priceChange1h": 0.32,
      "priceChange1d": -0.88,
      "priceChange1w": -1.9,
      "websiteUrl": "https://www.ethereum.org/",
      "twitterUrl": "https://twitter.com/ethereum",
      "contractAddress": "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2",
      "decimals": 18,
      "exp": [
        "https://etherscan.io/",
        "https://ethplorer.io/",
        "https://blockchair.com/ethereum",
        "https://eth.tokenview.io/"
      ]
    }
  ]
};
