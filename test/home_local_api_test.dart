import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:waddicryptowallet/src/data/common/module/shared_pref_module.dart';
import 'package:waddicryptowallet/src/data/home/local/api/home_local_api_impl.dart';
import 'package:waddicryptowallet/src/domain/home/adapters/home_local_api.dart';

class MockSharedPreferenceModule extends Mock
    implements SharedPreferenceModule {}

class HomeLocalApiMock extends Mock implements HomeLocalApi {}

void main() {
  group('Home Local Api Implementation test', () {
    late HomeLocalApiImpl homeLocalApi;
    late MockSharedPreferenceModule mockSharedPreferenceModule;

    setUp(() {
      mockSharedPreferenceModule = MockSharedPreferenceModule();
      homeLocalApi = HomeLocalApiImpl(mockSharedPreferenceModule);
    });

    test('getAssetsFavorites should return a list of favorite assets', () {
      // Arrange
      final expectedAssets = ['bitcoin', 'cardano,ripple'];
      when(mockSharedPreferenceModule.getAssetsFavorites())
          .thenReturn(expectedAssets);

      final result = homeLocalApi.getAssetsFavorites();

      expect(result, expectedAssets);
      verify(mockSharedPreferenceModule.getAssetsFavorites());
    });

    test(
        'saveAssetsFavorites should call saveAssetsFavorites method of SharedPreferenceModule',
        () {
      final assets = ['bitcoin', 'cardano,ripple'];

      homeLocalApi.saveAssetsFavorites(assets);

      verify(mockSharedPreferenceModule.saveAssetsFavorites(assets));
    });
  });
}
