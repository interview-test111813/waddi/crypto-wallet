import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:waddicryptowallet/src/data/common/interceptor/request_interceptor.dart';
import 'package:waddicryptowallet/src/data/common/module/network_module.dart';
import 'package:waddicryptowallet/src/data/common/module/shared_pref_module.dart';
import 'package:waddicryptowallet/src/data/home/local/api/home_local_api_impl.dart';
import 'package:waddicryptowallet/src/data/home/remote/api/home_api_impl.dart';
import 'package:waddicryptowallet/src/data/home/repository/home_repository_impl.dart';
import 'package:waddicryptowallet/src/domain/home/adapters/home_api.dart';
import 'package:waddicryptowallet/src/domain/home/adapters/home_local_api.dart';
import 'package:waddicryptowallet/src/domain/home/adapters/home_repository.dart';
import 'package:waddicryptowallet/src/domain/home/use_cases/get_assets_use_case.dart';
import 'package:waddicryptowallet/src/domain/home/use_cases/process_asset_to_faborite_use_case.dart';
import 'package:waddicryptowallet/src/presentation/common/dialogs/dialogs_controller.dart';
import 'package:waddicryptowallet/src/presentation/home/bloc/home_bloc.dart';
import 'package:waddicryptowallet/src/presentation/home/bloc/home_state_builder.dart';

GetIt sl = GetIt.instance;

Future<void> init() async {
  sl.registerSingletonAsync<SharedPreferences>(
      () => SharedPreferences.getInstance());

  sl.registerSingletonWithDependencies<SharedPreferenceModule>(
      () => SharedPreferenceModule(pref: sl<SharedPreferences>()),
      dependsOn: [SharedPreferences]);

  //interceptor
  sl.registerSingletonWithDependencies<RequestInterceptor>(
      () => RequestInterceptor(pref: sl()),
      dependsOn: [SharedPreferenceModule]);

  //module
  sl.registerLazySingleton<Dio>(
    () => NetworkModule(requestInterceptor: sl()).provideDio(),
  );

  sl.registerLazySingleton(
    () => DialogController(),
  );

  /*Apis*/
  sl.registerLazySingleton<HomeApi>(
    () => HomeApiImpl(
      dio: sl.call(), homeLocalApi: sl.call(),
    ),
  );
  sl.registerLazySingleton<HomeLocalApi>(
    () => HomeLocalApiImpl(
      sl.call(),
    ),
  );

  /*Repositories*/
  sl.registerLazySingleton<CryptoAssetsRepository>(
    () => HomeRepositoryImpl(
      homeApi: sl.call(),
    ),
  );

  /*Apis*/
  sl.registerLazySingleton<GetAssetsUseCase>(
    () => GetAssetsUseCase(repository: sl.call()),
  );

  sl.registerLazySingleton<ProcessAssetToFavoriteUseCase>(
    () => ProcessAssetToFavoriteUseCase(
      sl.call(),
    ),
  );

  //States Builders
  sl.registerLazySingleton(() => HomeStateBuilder());

  /* Blocs*/
  sl.registerFactory<HomeBloc>(() => HomeBloc(
    processAssetToFavoriteUseCase: sl.call(),
      dialogController: sl.call(),
      stateBuilder: sl.call(),
      getAssetsUseCase: sl.call()));
}
