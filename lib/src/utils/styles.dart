
import 'package:flutter/material.dart';
import 'package:waddicryptowallet/src/utils/colors.dart';
import 'package:waddicryptowallet/src/utils/common.dart';

class AppTextStyles {
  static TextStyle title2 = TextStyle(
    color: AppColors.darkPrimary,
    fontSize: SizeConfig.textMultiplier * 2,
    fontWeight: FontWeight.bold,
    fontFamily: 'Roboto',
  );


  static TextStyle buttonX1 = TextStyle(
    color: AppColors.darkPrimary,
    fontSize: SizeConfig.textMultiplier * 2,
    fontFamily: 'Roboto',
  );

  static TextStyle buttonX2 = TextStyle(
    color: AppColors.darkPrimary,
    fontSize: SizeConfig.textMultiplier * 3,
    fontFamily: 'Roboto',
  );

  static TextStyle buttonX3 = TextStyle(
    color: AppColors.darkPrimary,
    fontSize: SizeConfig.textMultiplier * 4,
    fontFamily: 'Roboto',
  );

  static TextStyle caption = TextStyle(
      color: AppColors.darkPrimary,
      fontSize: SizeConfig.textMultiplier * 1,
      fontFamily: 'Roboto');

  static TextStyle bottomBarItemStyle = const TextStyle(
    color: AppColors.gray900,
    fontSize: 12.0,
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w500,
  );

  static TextStyle bigHeadingStyle = const TextStyle(
    fontSize: 30.0,
    color: AppColors.black,
    fontFamily: 'Roboto',
    fontWeight: FontWeight.bold,
  );

  static TextStyle headingStyle = const TextStyle(
    fontSize: 18.0,
    color: AppColors.blue,
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w500,
  );

  static TextStyle greyHeadingStyle = const TextStyle(
    fontSize: 16.0,
    color: AppColors.gray900,
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w500,
  );

  static TextStyle blueTextStyle = const TextStyle(
    fontSize: 18.0,
    color: Colors.blue,
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w400,
  );

  static TextStyle whiteHeadingStyle = const TextStyle(
    fontSize: 22.0,
    color: AppColors.white,
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w500,
  );

  static TextStyle whiteSubHeadingStyle = const TextStyle(
    fontSize: 14.0,
    color: AppColors.white,
    fontFamily: 'Roboto',
    fontWeight: FontWeight.normal,
  );

  static TextStyle wbuttonWhiteTextStyle = const TextStyle(
    fontSize: 16.0,
    color: AppColors.white,
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w500,
  );

  static TextStyle buttonBlackTextStyle = const TextStyle(
    fontSize: 16.0,
    color: AppColors.black,
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w500,
  );

  static TextStyle moreStyle = const TextStyle(
    fontSize: 14.0,
    color: AppColors.primary,
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w500,
  );

  static TextStyle priceStyle = const TextStyle(
    fontSize: 18.0,
    color: AppColors.primary,
    fontFamily: 'Roboto',
    fontWeight: FontWeight.bold,
  );

  static TextStyle lightGreyStyle = TextStyle(
    fontSize: 15.0,
    color: Colors.grey.withOpacity(0.6),
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w500,
  );

// List Item Style Start
  static TextStyle listItemTitleStyle = const TextStyle(
    fontSize: 15.0,
    color: AppColors.black,
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w500,
  );
  static TextStyle listItemSubTitleStyle = const TextStyle(
    fontSize: 14.0,
    color: AppColors.gray900,
    fontFamily: 'Roboto',
    fontWeight: FontWeight.normal,
  );

// List Item Style End

// AppBar Style Start
  static TextStyle appbarHeadingStyle = const TextStyle(
    color: AppColors.dark,
    fontSize: 14.0,
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w500,
  );
  static TextStyle appbarSubHeadingStyle = const TextStyle(
    color: AppColors.white,
    fontSize: 13.0,
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w500,
  );

// AppBar Style End

// Search text style start
  static TextStyle searchTextStyle = TextStyle(
    color: AppColors.white.withOpacity(0.6),
    fontSize: 16.0,
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w500,
  );
}
