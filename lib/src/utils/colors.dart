import 'package:flutter/material.dart';

class AppColors {
  static const primary = Color(0xFF0D6EFD); // Bootstrap Blue
  static const secondary = Color(0xFF6C757D); // Bootstrap Gray 600
  static const success = Color(0xFF198754); // Bootstrap Green
  static const danger = Color(0xFFDC3545); // Bootstrap Red
  static const warning = Color(0xFFFFC107); // Bootstrap Yellow
  static const info = Color(0xFF0DCAF0); // Bootstrap Cyan
  static const light = Color(0xFFF8F9FA); // Bootstrap Gray 100
  static const darkPrimary = Color(0xFF0F2C45); // Bootstrap Gray 900
  static const dark = Color(0xFF212529); // Bootstrap Gray 900
  static const white = Color(0xFFFFFFFF); // Bootstrap White
  static const black = Color(0xFF000000); // Bootstrap Black
  static const gray100 = Color(0xFFF8F9FA); // Bootstrap Gray 100
  static const gray200 = Color(0xFFE9ECEF); // Bootstrap Gray 200
  static const gray300 = Color(0xFFDEE2E6); // Bootstrap Gray 300
  static const gray400 = Color(0xFFCED4DA); // Bootstrap Gray 400
  static const gray500 = Color(0xFFADB5BD); // Bootstrap Gray 500
  static const gray600 = Color(0xFF6C757D); // Bootstrap Gray 600
  static const gray700 = Color(0xFF495057); // Bootstrap Gray 700
  static const gray800 = Color(0xFF343A40); // Bootstrap Gray 800
  static const gray900 = Color(0xFF212529); // Bootstrap Gray 900
  static const blue = Color(0xFF0D6EFD); // Bootstrap Blue
  static const indigo = Color(0xFF6610F2); // Bootstrap Indigo
  static const purple = Color(0xFF6F42C1); // Bootstrap Purple
  static const pink = Color(0xFFD63384); // Bootstrap Pink
  static const red = Color(0xFFDC3545); // Bootstrap Red
  static const orange = Color(0xFFFFA500); // Bootstrap Orange
  static const yellow = Color(0xFFFFC107); // Bootstrap Yellow
  static const green = Color(0xFF198754); // Bootstrap Green
  static const teal = Color(0xFF20C997); // Bootstrap Teal
  static const cyan = Color(0xFF0DCAF0); // Bootstrap Cyan


}


class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF$hexColor";
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

