import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:waddicryptowallet/src/utils/common.dart';

final formatConcurrency = NumberFormat.simpleCurrency(locale: 'en');

const String emptyString = " ";

final double fixPadding = SizeConfig.heightMultiplier * 1.0;
EdgeInsets fixPaddingEdgeInsets =
    EdgeInsets.all(SizeConfig.heightMultiplier * 1.0);
EdgeInsets fixPaddingEdgeInsetsX2 =
    EdgeInsets.all(SizeConfig.heightMultiplier * 2.0);

SizedBox widthSpace = SizedBox(width: SizeConfig.widthMultiplier * 1.0);
SizedBox widthSpaceX2 = SizedBox(width: SizeConfig.heightMultiplier * 1.0);

SizedBox heightSpace = SizedBox(height: SizeConfig.heightMultiplier * 1.0);
SizedBox heightSpaceX2 = SizedBox(height: SizeConfig.heightMultiplier * 2.0);
SizedBox heightSpaceX3 = SizedBox(height: SizeConfig.heightMultiplier * 3.0);

const splashImageUrl =
    "https://lh6.googleusercontent.com/0ch69_KlsEBhqqbA7gKTRlZsol_bOjC9bTSplU3eRQjqL-JY3VG40Uy67b6s4L179NyBcACmz3FjkelhlwmJNAXG9qOK6KZ3bD4aBhJmuuryTCDsln3-mh0F7YvjdFvUZd0_Snz74hulhkTL";
