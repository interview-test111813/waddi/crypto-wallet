part of 'home_bloc.dart';

enum AssetsSectionView { assets, favorites }

class HomeState {
  Object? error;
  List<CoinModel> assets;
  AssetOrderBy assetOrderBy;
  AssetsSectionView assetsSectionView;

  HomeState(
      {this.assets = const [],
      this.assetOrderBy = AssetOrderBy.name,
      this.assetsSectionView = AssetsSectionView.assets,
      this.error});

  HomeState copyWith(
          {List<CoinModel>? assets,
          AssetOrderBy? assetOrderBy,
          AssetsSectionView? assetsSectionView,
          Object? error}) =>
      HomeState(
          assets: assets ?? this.assets,
          assetOrderBy: assetOrderBy ?? this.assetOrderBy,
          assetsSectionView: assetsSectionView ?? this.assetsSectionView,
          error: error ?? this.error);

  List<CoinModel> getFavorites() {
    return assets.where((element) => element.fab == true).toList();
  }
}
