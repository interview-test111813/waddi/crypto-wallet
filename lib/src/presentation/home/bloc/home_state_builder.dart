import 'package:waddicryptowallet/src/domain/home/entities/asset_order_enum.dart';
import 'package:waddicryptowallet/src/domain/home/entities/coin_model.dart';
import 'package:waddicryptowallet/src/domain/home/mixins/coin_model_mixin.dart';
import 'package:waddicryptowallet/src/presentation/home/bloc/home_bloc.dart';

class HomeStateBuilder with CoinsMixin {
  HomeState _state = HomeState();

  HomeStateBuilder();

  HomeStateBuilder setAssets(List<CoinModel> assets) {
    _state.assets = _sort(assets);
    return this;
  }

  HomeStateBuilder setAssetsView({required AssetsSectionView view}) {
    _state.assetsSectionView = view;
    return this;
  }

  HomeStateBuilder sortAssetBy(AssetOrderBy sort) {
    _state = _state.copyWith(
        assetOrderBy: sort, assets: _sort(_state.assets.toList()));
    return this;
  }

  HomeStateBuilder addOrRemoveFromFavorites(CoinModel asset) {
    _state.assets.where((element) => element == asset).first.fab = !asset.fab;
    return this;
  }

  HomeState error(Object e) {
    return _state.copyWith(error: e);
  }

  HomeState build() {
    return _state;
  }

  _sort(List<CoinModel> assets) {
    if (_state.assetOrderBy == AssetOrderBy.name) {
      return sortByName(assets);
    }

    return sortByPrice(assets);
  }
}
