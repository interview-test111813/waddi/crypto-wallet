part of 'home_bloc.dart';

@immutable
abstract class HomeEvent {}

class HomeGetAssetsEvent extends HomeEvent {}

class HomeEventSortBy extends HomeEvent {
  final AssetOrderBy assetOrderBy;

  HomeEventSortBy({required this.assetOrderBy});
}

class HomeEventHandleFavorites extends HomeEvent {
  final CoinModel asset;

  HomeEventHandleFavorites({required this.asset});
}

class HomeEventChangeAssetSectionView extends HomeEvent {
  final AssetsSectionView view;

  HomeEventChangeAssetSectionView({
    required this.view,
  });
}
