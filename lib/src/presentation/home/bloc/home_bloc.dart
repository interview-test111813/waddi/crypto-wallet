import 'dart:async';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:bloc/bloc.dart';
import 'package:waddicryptowallet/src/domain/home/entities/asset_order_enum.dart';
import 'package:waddicryptowallet/src/domain/home/entities/coin_model.dart';
import 'package:waddicryptowallet/src/domain/home/mixins/coin_model_mixin.dart';
import 'package:waddicryptowallet/src/domain/home/use_cases/get_assets_use_case.dart';
import 'package:waddicryptowallet/src/domain/home/use_cases/process_asset_to_faborite_use_case.dart';
import 'package:waddicryptowallet/src/presentation/common/dialogs/dialogs_controller.dart';
import 'package:waddicryptowallet/src/presentation/home/bloc/home_state_builder.dart';
import 'package:waddicryptowallet/src/utils/common.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> with CoinsMixin {
  late final HomeStateBuilder _stateBuilder;
  final DialogController dialogController;

  Stream<List<CoinModel>> get assetsStream => _assetsStreamController.stream;

  final StreamController<List<CoinModel>> _assetsStreamController =
      StreamController<List<CoinModel>>.broadcast();

  set addAssets(List<CoinModel> assets) => _assetsStreamController.add(assets);

  final GetAssetsUseCase _getAssetsUseCase;
  final ProcessAssetToFavoriteUseCase _processAssetToFavoriteUseCase;

  List<CoinModel> _coins = [];

  HomeBloc(
      {required ProcessAssetToFavoriteUseCase processAssetToFavoriteUseCase,
      required this.dialogController,
      required HomeStateBuilder stateBuilder,
      required GetAssetsUseCase getAssetsUseCase})
      : _getAssetsUseCase = getAssetsUseCase,
        _stateBuilder = stateBuilder,
        _processAssetToFavoriteUseCase = processAssetToFavoriteUseCase,
        super(stateBuilder.build()) {
    on<HomeGetAssetsEvent>(
        (event, emit) async => handleOnHomeGetAssetsEvent(event, emit));

    on<HomeEventSortBy>((event, emit) => handleOnHomeEventSortBy(event, emit));

    on<HomeEventHandleFavorites>(
        (event, emit) => handleOnHomeHandleFavoritesEvent(event, emit));

    on<HomeEventChangeAssetSectionView>(
        (event, emit) => handleHomeEventSetAssetSectionView(event, emit));
  }

  /*Handles
  *
  * */
  Future<void> handleOnHomeGetAssetsEvent(
      HomeGetAssetsEvent event, Emitter<HomeState> emit) async {
    final response = await _getAssetsUseCase.invoke();

    response.fold((l) {
      _coins = updateCoinListInformation(
        _coins,
        l,
      );

      emit(_stateBuilder.setAssets(_coins).build());

      addAssets = state.assets;
    }, (r) {
      emit(
        _stateBuilder.error(r),
      );
      return;
    });
  }

  Future<void> handleOnHomeEventSortBy(
      HomeEventSortBy event, Emitter<HomeState> emit) async {
    emit(_stateBuilder.sortAssetBy(event.assetOrderBy).build());
    addAssets = state.assets;
  }

  Future<void> handleOnHomeHandleFavoritesEvent(
      HomeEventHandleFavorites event, Emitter<HomeState> emit) async {
    emit(_stateBuilder.addOrRemoveFromFavorites(event.asset).build());
    if (state.assetsSectionView == AssetsSectionView.favorites) {
      addAssets = state.getFavorites();
    } else {
      addAssets = state.assets;
    }

    _processAssetToFavoriteUseCase
        .invoke(state.getFavorites().map((e) => e.id).toList());
  }

  Future handleHomeEventSetAssetSectionView(
      HomeEventChangeAssetSectionView event, Emitter<HomeState> emit) async {
    if (event.view == AssetsSectionView.favorites) {
      addAssets = state.getFavorites();
    } else {
      addAssets = state.assets;
    }

    emit(_stateBuilder.setAssetsView(view: event.view).build());
  }
}
