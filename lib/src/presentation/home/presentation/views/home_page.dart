import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:get/get.dart';
import 'package:waddicryptowallet/src/presentation/home/bloc/home_bloc.dart';
import 'package:waddicryptowallet/src/presentation/home/presentation/views/assets_view.dart';
import 'package:waddicryptowallet/src/presentation/home/presentation/widgets/assets_navigation_bar_widget.dart';
import 'package:waddicryptowallet/src/utils/colors.dart';
import 'package:waddicryptowallet/src/utils/common.dart';
import 'package:waddicryptowallet/src/utils/consts.dart';
import 'package:waddicryptowallet/src/utils/styles.dart';

import '../widgets/home_balance_and_options_widget.dart';

class HomePage extends HookWidget {
  const HomePage({super.key});

  void useInterval(VoidCallback callback, Duration delay) {
    final savedCallback = useRef(callback);
    savedCallback.value = callback;

    useEffect(() {
      final timer = Timer.periodic(delay, (_) => savedCallback.value());
      return timer.cancel;
    }, [delay]);
  }

  @override
  Widget build(BuildContext context) {
    useEffect(
      () {
        BlocProvider.of<HomeBloc>(context).add(HomeGetAssetsEvent());

        return () {};
      },
      [],
    );
    useInterval(
      () async {
        BlocProvider.of<HomeBloc>(context).add(HomeGetAssetsEvent());
      },
      const Duration(seconds: 60),
    );
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {},
            icon: const Icon(
              Icons.settings,
              color: AppColors.darkPrimary,
            ),
          ),
        ],
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Column(
        children: [
          const HomeBalanceAndOptionsWidget(),
          heightSpaceX3,
          const Divider(),
          BlocBuilder<HomeBloc, HomeState>(builder: (context, state) {
            if (state.error == null) {
              return Expanded(
                child: Column(
                  children: const [
                    AssetsNavigationBarWidget(),
                    Expanded(child: AssetsListView()),
                  ],
                ),
              );
            }
            return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  heightSpaceX3,
                  heightSpaceX3,
                  Icon(
                    Icons.error,
                    color: AppColors.red,
                    size: SizeConfig.imageSizeMultiplier * 20,
                  ),
                  heightSpace,
                  heightSpace,
                  Text(
                    "Network error, we'll try again in a moment.",
                    style: AppTextStyles.buttonX1,
                  )
                ]);
          })
        ],
      ),
    );
  }
}
