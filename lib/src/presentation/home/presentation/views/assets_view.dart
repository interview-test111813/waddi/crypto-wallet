import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shimmer/shimmer.dart';
import 'package:waddicryptowallet/src/domain/home/entities/coin_model.dart';
import 'package:waddicryptowallet/src/presentation/home/bloc/home_bloc.dart';
import 'package:waddicryptowallet/src/presentation/home/presentation/widgets/assets_list_widget.dart';
import 'package:waddicryptowallet/src/presentation/home/presentation/widgets/button_order_by_widget.dart';
import 'package:waddicryptowallet/src/utils/colors.dart';
import 'package:waddicryptowallet/src/utils/common.dart';
import 'package:waddicryptowallet/src/utils/consts.dart';

class AssetsListView extends StatelessWidget {
  const AssetsListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Align(
            alignment: Alignment.topRight, child: ButtonOrderByWidget()),
        Expanded(
            child: StreamBuilder<List<CoinModel>>(
                stream: context.read<HomeBloc>().assetsStream,
                builder: (_, snapshot) {
                  if (snapshot.hasData) {
                    return AssetsListWidget(coinList: snapshot.data!.toList());
                  } else if (snapshot.hasError) {
                    return Text('Error: ${snapshot.error}');
                  } else {
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: SizeConfig.widthMultiplier * 50,
                          child: const LinearProgressIndicator(
                            color: AppColors.darkPrimary,
                            backgroundColor: AppColors.blue,
                          ),
                        ),
                        heightSpaceX2,
                        const Text("please wait!")
                      ],
                    );
                  }
                }))
      ],
    );
  }
}
