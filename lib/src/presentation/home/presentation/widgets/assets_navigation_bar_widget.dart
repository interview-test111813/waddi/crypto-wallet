import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:waddicryptowallet/src/presentation/home/bloc/home_bloc.dart';
import 'package:waddicryptowallet/src/utils/colors.dart';

class AssetsNavigationBarWidget extends HookWidget {
  const AssetsNavigationBarWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final assetsSectionView =
        useState(context.read<HomeBloc>().state.assetsSectionView);

    return BottomNavigationBar(
        elevation: 0,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            label: 'Coins',
            backgroundColor: Colors.red,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'My Favorites',
            backgroundColor: Colors.red,
          ),
        ],
        currentIndex: assetsSectionView.value.index,
        selectedItemColor: AppColors.blue,
        onTap: (e) {
          assetsSectionView.value = AssetsSectionView.values[e];

          context.read<HomeBloc>().add(
                HomeEventChangeAssetSectionView(
                  view: AssetsSectionView.values[e],
                ),
              );
        });
  }
}
