import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:waddicryptowallet/injection_container.dart';
import 'package:waddicryptowallet/src/domain/home/entities/coin_model.dart';
import 'package:waddicryptowallet/src/presentation/common/dialogs/dialogs_controller.dart';
import 'package:waddicryptowallet/src/presentation/home/bloc/home_bloc.dart';
import 'package:waddicryptowallet/src/utils/colors.dart';
import 'package:waddicryptowallet/src/utils/common.dart';
import 'package:waddicryptowallet/src/utils/consts.dart';
import 'package:waddicryptowallet/src/utils/styles.dart';

import 'crypto_asset_widget.dart';

class AssetsListWidget extends StatelessWidget {
  final List<CoinModel> coinList;
  const AssetsListWidget({Key? key, required this.coinList}) : super(key: key);

  Future<bool?> _showDeleteSnackBar(BuildContext context) async {
    bool delete = true;
    final snackBarController = ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        duration: const Duration(
          seconds: 1,
        ),
        backgroundColor: AppColors.darkPrimary,
        content: const Text(
          'Added to favorites',
        ),
        action: SnackBarAction(
          label: "undo",
          onPressed: () => delete = false,
        ),
      ),
    );
    await snackBarController.closed;

    return delete;
  }

  Widget _dismissibleFavorites(bool isFavorite) {
    return Container(
      color: isFavorite ? AppColors.red : AppColors.green,
      child: Align(
        alignment: Alignment.centerRight,
        child: Padding(
          padding: const EdgeInsets.only(
            right: 16,
          ),
          child: Row(
            children: [
              Expanded(
                child: Text(
                  isFavorite ? "Remove from favorites": "Add to favorites",
                  style:
                      AppTextStyles.buttonX1.copyWith(color: AppColors.white),
                  textAlign: TextAlign.end,
                ),
              ),
              widthSpace,
              if (!isFavorite)
                const Icon(
                  Icons.star,
                  color: AppColors.yellow,
                ),
            ],
          ),
        ),
      ),
    );
  }

  Future<bool> _handleConfirmDismiss(
      DismissDirection direction, BuildContext context, CoinModel asset) async {
    final DialogController dialogController = sl.call()..show();

    if (direction == DismissDirection.startToEnd) {
      return false;
    } else {
      final response = await _showDeleteSnackBar(context);
      dialogController.hide();
      if (response == true && context.mounted) {
        BlocProvider.of<HomeBloc>(context)
            .add(HomeEventHandleFavorites(asset: asset));
      } else {
        return false;
      }
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    if (coinList.isNotEmpty) {
      return ListView.builder(
        itemCount: coinList.length,
        itemBuilder: (context, i) => Dismissible(
          key: UniqueKey(),
          background: Container(
            color: AppColors.dark,
          ),
          secondaryBackground: _dismissibleFavorites(coinList[i].fab),
          confirmDismiss: (direction) =>
              _handleConfirmDismiss(direction, context, coinList[i]),
          child: Column(
            children: [
              CryptoAssetWidget(
                coinModel: coinList[i],
              ),
              Padding(
                padding: EdgeInsets.only(left: SizeConfig.widthMultiplier * 20),
                child: const Divider(),
              ),
            ],
          ),
        ),
      );
    } else {
      return const Text("Noting to show");
    }
  }
}
