import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:waddicryptowallet/src/domain/home/entities/options_model.dart';
import 'package:waddicryptowallet/src/utils/colors.dart';
import 'package:waddicryptowallet/src/utils/consts.dart';
import 'package:waddicryptowallet/src/utils/styles.dart';

class HomeBalanceAndOptionsWidget extends StatelessWidget {
  const HomeBalanceAndOptionsWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          formatConcurrency.format(0.00),
          style: AppTextStyles.bigHeadingStyle
              .copyWith(color: AppColors.darkPrimary),
        ),
        heightSpace,
        const Text('Main Wallet'),
        heightSpaceX2,
        Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: HomeHeaderOptionModel.options
                .map((e) => Column(children: [
                      MaterialButton(
                          onPressed: () {},
                          color: Colors.blue,
                          textColor: Colors.white,
                          padding: const EdgeInsets.all(16),
                          shape: const CircleBorder(),
                          child: e.icon),
                      heightSpace,
                      Text(e.name!.capitalize!,
                          style: AppTextStyles.buttonX1
                              .copyWith(fontWeight: FontWeight.bold))
                    ]))
                .toList())
      ],
    );
  }
}
