import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:waddicryptowallet/generated/l10n.dart';
import 'package:waddicryptowallet/src/domain/home/entities/asset_order_enum.dart';
import 'package:waddicryptowallet/src/presentation/home/bloc/home_bloc.dart';
import 'package:waddicryptowallet/src/utils/colors.dart';
import 'package:waddicryptowallet/src/utils/common.dart';
import 'package:waddicryptowallet/src/utils/styles.dart';

class ButtonOrderByWidget extends StatefulWidget {
  const ButtonOrderByWidget({super.key});

  @override
  State<ButtonOrderByWidget> createState() => _ButtonOrderByWidgetState();
}

class _ButtonOrderByWidgetState extends State<ButtonOrderByWidget> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: SizeConfig.widthMultiplier * 25,
      child: TextButton(
        style: TextButton.styleFrom(foregroundColor: AppColors.darkPrimary),
        onPressed: () {
          final state = context.read<HomeBloc>().state;
          Get.bottomSheet(
            StatefulBuilder(
              builder: (context, setState) => Container(
                // height: 150,
                color: AppColors.white,
                child: Column(children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            'Order By',
                            style: AppTextStyles.title2,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const Divider(),
                  ...AssetOrderBy.values
                      .map((e) => Row(children: [
                            Checkbox(
                              value: state.assetOrderBy == e ? true : false,
                              onChanged: (value) {
                                setState(() {
                                  state.assetOrderBy = e;
                                });

                                context.read<HomeBloc>().add(HomeEventSortBy(
                                      assetOrderBy: e,
                                    ));
                                Get.back();
                              },
                            ),
                            Text(e.name.toString(),
                                style: AppTextStyles.buttonX1)
                          ]))
                      .toList(),
                ]),
              ),
            ),
            barrierColor: Colors.transparent,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
                side: const BorderSide(width: 0.51, color: Colors.black)),
            enableDrag: false,
          );
        },
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Icon(
                Icons.arrow_drop_down,
              ),
              BlocBuilder<HomeBloc, HomeState>(
                  // buildWhen: (previous, current) =>
                  //     previous.assetsSectionView != current.assetsSectionView,
                  builder: (_, state) {
                return Text(state.assetOrderBy.name);
              })
            ]),
      ),
    );
  }
}
