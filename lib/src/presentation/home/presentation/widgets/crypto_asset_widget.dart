import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:waddicryptowallet/src/domain/home/entities/coin_model.dart';
import 'package:waddicryptowallet/src/utils/colors.dart';
import 'package:waddicryptowallet/src/utils/common.dart';
import 'package:waddicryptowallet/src/utils/consts.dart';

class CryptoAssetWidget extends StatelessWidget {
  final CoinModel coinModel;
  const CryptoAssetWidget({super.key, required this.coinModel});

  TextSpan priceChangeTextWidget(double percentage) {
    Color color = AppColors.green;
    String operator = '+';

    if (percentage.isNegative) {
      color = AppColors.red;
      String operator = '-';
    }

    return TextSpan(
      text: '$operator${percentage.toString()}%',
      style: TextStyle(fontWeight: FontWeight.bold, color: color),
    );
  }

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {},
      child: ListTile(
        title: Row(
          children: [
            if (coinModel.fab)
              Icon(Icons.star,
                  color: AppColors.yellow,
                  size: SizeConfig.imageSizeMultiplier * 5),
            widthSpace,
            Text(coinModel.name.capitalize()),
          ],
        ),
        subtitle: RichText(
          text: TextSpan(
            text: '\$${coinModel.price.toStringAsFixed(2)} ',
            style: DefaultTextStyle.of(context).style,
            children: <TextSpan>[
              priceChangeTextWidget(coinModel.priceChange1d!)
            ],
          ),
        ),
        leading: CachedNetworkImage(
          imageUrl: coinModel.icon,
          progressIndicatorBuilder: (context, url, downloadProgress) =>
              CircularProgressIndicator(value: downloadProgress.progress),
          errorWidget: (context, url, error) => const Icon(Icons.error),
        ),
        trailing: Text(
          '${coinModel.symbol} 0',
        ),
      ),
    );
  }
}
