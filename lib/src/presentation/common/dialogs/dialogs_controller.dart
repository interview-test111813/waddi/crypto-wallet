import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sn_progress_dialog/sn_progress_dialog.dart';
import 'package:waddicryptowallet/generated/l10n.dart';
import 'package:waddicryptowallet/src/utils/colors.dart';
import 'package:waddicryptowallet/src/utils/consts.dart';

ProgressDialog pr = ProgressDialog(context: Get.context);

class DialogController {
  Future<void> hide() async {
    if (pr.isOpen()) pr.close();
  }

  Future<void> show({String? description}) async {
    String msj = '${S.current.wait}...';
    if (!pr.isOpen()) {
      if (description != null) {
        msj = "$description ...";
      }

      await pr.show(
        max: 100,
        progressType: ProgressType.normal,
        msg: msj,
        barrierDismissible: false,
        barrierColor: AppColors.black.withOpacity(0.9),
        elevation: 1,
        progressValueColor: AppColors.darkPrimary,
        msgColor: AppColors.darkPrimary,
      );
    }
  }


  Future showHttpError() {
    return Get.dialog(
      AlertDialog(
        backgroundColor: AppColors.gray400,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(
            20.0,
          ),
        ),
        title: Center(
            child: Text(
              S.current.dialogShowHttpErrorTitle,
              textAlign: TextAlign.center,
            )),
        content: const Icon(
          Icons.signal_cellular_connected_no_internet_4_bar,
          size: 70,
          color: AppColors.blue,
        ),
        contentPadding: fixPaddingEdgeInsets,
        actions: <Widget>[
          Center(
            child: Padding(
              padding: fixPaddingEdgeInsets,
              child: Text(
                S.current.dialogShowHttpErrorSub,
                textAlign: TextAlign.center,
              ),
            ),
          ),
          SizedBox(
            width: MediaQuery.of(Get.context!).size.width,
            child: TextButton(
              onPressed: () => Get.back(),
              style: ElevatedButton.styleFrom(
                backgroundColor: AppColors.darkPrimary,
                textStyle: const TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              child: Text(
                S.current.accept,
                style: const TextStyle(
                  color: AppColors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

}
