import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:waddicryptowallet/generated/l10n.dart';
import 'package:waddicryptowallet/routes/routes.dart';
import 'package:waddicryptowallet/src/utils/colors.dart';
import 'package:waddicryptowallet/src/utils/common.dart';
import 'package:waddicryptowallet/src/utils/consts.dart';
import 'package:waddicryptowallet/src/utils/styles.dart';

class SplashPage extends HookWidget {
  const SplashPage({super.key});

  @override
  Widget build(BuildContext context) {
    useEffect(
      () {
        Future.delayed(const Duration(seconds: 4),
            () => Get.offAndToNamed(GlobalRouter.routeRoot));

        return () {};
      },
      [],
    );

    return Scaffold(
      body: Stack(
        children: [
          Positioned.fill(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: SizeConfig.widthMultiplier * 40,
                  child: Image.network(splashImageUrl),
                ),
                heightSpaceX2,
                heightSpaceX2,
                Center(
                  child: Text(S.current.appName,
                      style: AppTextStyles.bigHeadingStyle),
                ),
                heightSpace,
                heightSpace,
                heightSpace,
                heightSpace,
                const SpinKitPulse(
                  color: AppColors.blue,
                  size: 50.0,
                ),
              ],
            ),
          ),
          Positioned(
            bottom: 0,
            right: 0,
            left: 0,
            child: Padding(
              padding: fixPaddingEdgeInsets.copyWith(bottom: 10, top: 0),
              child: Center(
                child: Text(
                  S.current.splashFooter(2023),
                  style: AppTextStyles.appbarHeadingStyle,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
