import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:waddicryptowallet/src/data/common/module/shared_pref_module.dart';
import 'package:waddicryptowallet/src/utils/common.dart';

class RequestInterceptor extends Interceptor {
  final SharedPreferenceModule pref;

  RequestInterceptor({required this.pref});

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    String userData = pref.getUserData();
    if(userData.isNotEmpty){
      String token = jsonDecode(userData)['token'];
      options.headers["Authorization"] = token;
    }
    return super.onRequest(options, handler);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    Logger.write("=== Dio Error Occured ===",isError: true);
     Logger.write(err.message.toString(),isError: true);
    return super.onError(err, handler);

  }
}