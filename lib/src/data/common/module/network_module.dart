import 'package:dio/dio.dart';
import 'package:waddicryptowallet/src/data/common/interceptor/request_interceptor.dart';

class NetworkModule {
  final Dio _dio = Dio();
  final String _baseUrl =
      "https://api.coinstats.app/";
  final RequestInterceptor requestInterceptor;

  NetworkModule({required this.requestInterceptor});

  BaseOptions _dioOptions() {
    BaseOptions opts = BaseOptions();
    opts.baseUrl = _baseUrl;
    opts.connectTimeout = const Duration(milliseconds: 60000);
    opts.receiveTimeout = const Duration(milliseconds: 60000);
    opts.sendTimeout = const Duration(milliseconds: 60000);
    return opts;
  }

  Dio provideDio() {
    _dio.options = _dioOptions();
    _dio.interceptors.add(requestInterceptor);
    return _dio;
  }
}
