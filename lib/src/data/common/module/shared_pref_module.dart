// ignore_for_file: constant_identifier_names

import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceModule {
  final SharedPreferences pref;
  static const String _PREF_USER = "user_data";
  static const String _PREF_ASSETS_USER = "user_preferences_assets";

  SharedPreferenceModule({required this.pref});

  void clear() => pref.clear();

  void saveUserData(String userDataInJson) =>
      pref.setString(_PREF_USER, userDataInJson);

  String getUserData() {
    String userDataInJson = pref.getString(_PREF_USER) ?? "";
    return userDataInJson;
  }

  getAssetsFavorites() {
    final List<String> data = pref.getStringList(_PREF_ASSETS_USER) ?? [];
    return data;
  }

  saveAssetsFavorites(List<String> assets) {
    pref.setStringList(
      _PREF_ASSETS_USER,
      assets,
    );
  }
}
