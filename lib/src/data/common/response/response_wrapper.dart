import 'package:waddicryptowallet/src/data/common/util/serializable.dart';

class WrappedResponse<T extends Serializable> {
  int status;
  String message;
  T? data;

  WrappedResponse(
      {required this.status, required this.message, required this.data});

  factory WrappedResponse.fromJson(
      Map<String, dynamic> json, Function(Map<String, dynamic>) create) {
    return WrappedResponse<T>(
        status: json["status"],
        message: json["message"],
        data: create(json["data"]));
  }

  Map<String, dynamic> toJson() {
    return {
      "status": this.status,
      "message": this.message,
      "data": this.data?.toJson()
    };
  }
}

class WrappedListResponse<T extends Serializable> {
  List<T>? data;

  WrappedListResponse({required this.data});

  factory WrappedListResponse.fromJson(
      Map<String, dynamic> json, Function(List<dynamic>) create) {
    return WrappedListResponse(data: create(json["coins"]));
  }

  Map<String, dynamic> toJson() {
    return {"data": this.data};
  }
}
