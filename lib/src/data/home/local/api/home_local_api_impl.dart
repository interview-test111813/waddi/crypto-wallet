import 'package:waddicryptowallet/src/data/common/module/shared_pref_module.dart';
import 'package:waddicryptowallet/src/domain/home/adapters/home_local_api.dart';

class HomeLocalApiImpl implements HomeLocalApi {
  final SharedPreferenceModule sharedPreferenceModule;

  HomeLocalApiImpl(this.sharedPreferenceModule);

  @override
  List<String> getAssetsFavorites() {
    return sharedPreferenceModule.getAssetsFavorites();
  }

  @override
  saveAssetsFavorites(List<String> assets) {
    sharedPreferenceModule.saveAssetsFavorites(assets);
  }
}
