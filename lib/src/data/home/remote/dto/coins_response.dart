import 'package:waddicryptowallet/src/data/common/util/serializable.dart';
import 'package:waddicryptowallet/src/utils/parse_utils.dart';

class CoinModelResponse extends Serializable {
  String id;
  String icon;
  String name;
  String symbol;
  int? rank;
  double price;
  double? priceBtc;
  double? volume;
  double? marketCap;
  double? availableSupply;
  double? totalSupply;
  double? priceChange1h;
  double? priceChange1d;
  double? priceChange1w;
  String? websiteUrl;
  String? twitterUrl;
  List<String>? exp;
  String? contractAddress;
  int? decimals;

  CoinModelResponse(
      {required this.id,
      required this.icon,
      required this.name,
      required this.symbol,
      required this.rank,
      required this.price,
      required this.priceBtc,
      required this.volume,
      required this.marketCap,
      required this.availableSupply,
      required this.totalSupply,
      required this.priceChange1h,
      required this.priceChange1d,
      required this.priceChange1w,
      required this.websiteUrl,
      required this.twitterUrl,
      required this.exp,
      required this.contractAddress,
      required this.decimals});

  CoinModelResponse.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        icon = json['icon'],
        name = json['name'],
        symbol = json['symbol'],
        rank = json['rank'],
        price = ParseUtils.parseDouble(json['price'].toString()),
        priceBtc = ParseUtils.parseDouble(json['priceBtc'].toString()),
        volume = ParseUtils.parseDouble(json['volume'].toString()),
        marketCap = ParseUtils.parseDouble(json['marketCap'].toString()),
        availableSupply = ParseUtils.parseDouble(json['availableSupply'].toString()),
        totalSupply = ParseUtils.parseDouble(json['totalSupply'].toString()),
        priceChange1h =
            ParseUtils.parseDouble(json['priceChange1h'].toString()),
        priceChange1d =
            ParseUtils.parseDouble(json['priceChange1d'].toString()),
        priceChange1w =
            ParseUtils.parseDouble(json['priceChange1w'].toString()),
        websiteUrl = json['websiteUrl'],
        twitterUrl = json['twitterUrl'],
        exp = json['exp'].cast<String>(),
        contractAddress = json['contractAddress'],
        decimals = json['decimals'];

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['icon'] = icon;
    data['name'] = name;
    data['symbol'] = symbol;
    data['rank'] = rank;
    data['price'] = price;
    data['priceBtc'] = priceBtc;
    data['volume'] = volume;
    data['marketCap'] = marketCap;
    data['availableSupply'] = availableSupply;
    data['totalSupply'] = totalSupply;
    data['priceChange1h'] = priceChange1h;
    data['priceChange1d'] = priceChange1d;
    data['priceChange1w'] = priceChange1w;
    data['websiteUrl'] = websiteUrl;
    data['twitterUrl'] = twitterUrl;
    data['exp'] = exp;
    data['contractAddress'] = contractAddress;
    data['decimals'] = decimals;
    return data;
  }
}
