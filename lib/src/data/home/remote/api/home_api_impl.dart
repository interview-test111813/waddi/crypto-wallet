import 'package:dio/dio.dart';
import 'package:waddicryptowallet/src/data/common/module/shared_pref_module.dart';
import 'package:waddicryptowallet/src/data/common/response/response_wrapper.dart';
import 'package:waddicryptowallet/src/data/home/remote/dto/coins_response.dart';
import 'package:waddicryptowallet/src/domain/common/base/base_exception.dart';
import 'package:waddicryptowallet/src/domain/home/adapters/home_api.dart';
import 'package:waddicryptowallet/src/domain/home/adapters/home_local_api.dart';
import 'package:waddicryptowallet/src/domain/home/entities/coin_model.dart';

class HomeApiImpl implements HomeApi {
  final Dio _dio;

  final HomeLocalApi homeLocalApi;

  HomeApiImpl({required this.homeLocalApi, required Dio dio}) : _dio = dio;

  @override
  Future<List<CoinModel>> getCryptoAssets() async {
    try {
      final response = await _dio.get("public/v1/coins?",
          queryParameters: {"skip": 0, "limit": 100, "currency": "EUR"});
      if (response.statusCode == 200) {
        var converted = WrappedListResponse<CoinModelResponse>.fromJson(
            response.data, (data) {
          List<CoinModelResponse> coins =
              data.map((e) => CoinModelResponse.fromJson(e)).toList();
          return coins;
        });

        var favoritesList = homeLocalApi.getAssetsFavorites();
        var coins = CoinModel.mapResponseListToEntityList(
            converted.data!, favoritesList);
        return coins;
      }
      throw BaseException(message: "error", code: response.statusCode);
    } on DioError catch (e) {
      throw BaseException(
          message: e.message ?? "Network Error",
          code: e.response?.statusCode ?? 0);
    } on Exception catch (e) {
      throw BaseException(message: e.toString());
    }
  }
}
