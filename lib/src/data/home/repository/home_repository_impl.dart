import 'package:dartz/dartz.dart';
import 'package:waddicryptowallet/src/domain/common/base/base_exception.dart';
import 'package:waddicryptowallet/src/domain/common/base/base_failure.dart';
import 'package:waddicryptowallet/src/domain/home/adapters/home_api.dart';
import 'package:waddicryptowallet/src/domain/home/adapters/home_repository.dart';
import 'package:waddicryptowallet/src/domain/home/entities/coin_model.dart';
import 'package:waddicryptowallet/src/utils/common.dart';

class HomeRepositoryImpl implements CryptoAssetsRepository {
  final HomeApi _homeApi;

  HomeRepositoryImpl({required HomeApi homeApi}) : _homeApi = homeApi;

  @override
  Future<Either<List<CoinModel>, Failure>> getAssets() async {
    try {
      final coins = await _homeApi.getCryptoAssets();

      return left(coins);
    } on BaseException catch (e) {
      return Right(BaseFailure(code: e.code!, message: e.message));
    } catch (e) {
      Logger.write("text");

      rethrow;
    }
  }
}
