import 'package:waddicryptowallet/src/domain/home/adapters/home_local_api.dart';
import 'package:waddicryptowallet/src/utils/common.dart';

class ProcessAssetToFavoriteUseCase {
  final HomeLocalApi homeLocalApi;

  ProcessAssetToFavoriteUseCase(this.homeLocalApi);

  Future invoke(List<String> assets) async {
    homeLocalApi.saveAssetsFavorites(assets);
  }
}
