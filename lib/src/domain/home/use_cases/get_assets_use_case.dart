import 'package:dartz/dartz.dart';
import 'package:waddicryptowallet/src/domain/common/base/base_failure.dart';
import 'package:waddicryptowallet/src/domain/home/adapters/home_repository.dart';
import 'package:waddicryptowallet/src/domain/home/entities/coin_model.dart';

class GetAssetsUseCase {
  final CryptoAssetsRepository _repository;

  GetAssetsUseCase({required CryptoAssetsRepository repository})
      : _repository = repository;

  Future<Either<List<CoinModel>, Failure>> invoke() async {
    final result = await _repository.getAssets();
    return result;
  }
}
