import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:waddicryptowallet/src/data/common/util/serializable.dart';
import 'package:waddicryptowallet/src/data/home/remote/dto/coins_response.dart';

class CoinModel extends Serializable {
  bool fab;
  String id;
  String icon;
  String name;
  String symbol;
  int? rank;
  double price;
  double? priceBtc;
  double? volume;
  double? marketCap;
  double? availableSupply;
  double? totalSupply;
  double? priceChange1h;
  double? priceChange1d;
  double? priceChange1w;
  String? websiteUrl;
  String? twitterUrl;
  List<String>? exp;
  String? contractAddress;
  int? decimals;

  CoinModel(
      {required this.fab,
      required this.id,
      required this.icon,
      required this.name,
      required this.symbol,
      required this.rank,
      required this.price,
      required this.priceBtc,
      required this.volume,
      required this.marketCap,
      required this.availableSupply,
      required this.totalSupply,
      required this.priceChange1h,
      required this.priceChange1d,
      required this.priceChange1w,
      required this.websiteUrl,
      required this.twitterUrl,
      required this.exp,
      required this.contractAddress,
      required this.decimals});

  CoinModel.toEntity(CoinModelResponse coin, this.fab)
      : id = coin.id,
        icon = coin.icon,
        name = coin.name,
        symbol = coin.symbol,
        rank = coin.rank,
        price = coin.price,
        priceBtc = coin.priceBtc,
        volume = coin.volume,
        marketCap = coin.marketCap,
        availableSupply = coin.availableSupply,
        totalSupply = coin.totalSupply,
        priceChange1h = coin.priceChange1h,
        priceChange1d = coin.priceChange1d,
        priceChange1w = coin.priceChange1w,
        websiteUrl = coin.websiteUrl,
        twitterUrl = coin.twitterUrl,
        exp = coin.exp,
        contractAddress = coin.contractAddress,
        decimals = coin.decimals;

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['icon'] = icon;
    data['name'] = name;
    data['symbol'] = symbol;
    data['rank'] = rank;
    data['price'] = price;
    data['priceBtc'] = priceBtc;
    data['volume'] = volume;
    data['marketCap'] = marketCap;
    data['availableSupply'] = availableSupply;
    data['totalSupply'] = totalSupply;
    data['priceChange1h'] = priceChange1h;
    data['priceChange1d'] = priceChange1d;
    data['priceChange1w'] = priceChange1w;
    data['websiteUrl'] = websiteUrl;
    data['twitterUrl'] = twitterUrl;
    data['exp'] = exp;
    data['contractAddress'] = contractAddress;
    data['decimals'] = decimals;
    return data;
  }

  static mapResponseListToEntityList(
      List<CoinModelResponse> coins, List<String> favorites) {
    return coins.map((e) {
      final fab = favorites.where((element) => element == e.id).isNotEmpty;

      return CoinModel.toEntity(e, fab);
    }).toList();
  }

  @override
  int get hashCode => Object.hash(
        id,
        icon,
        name,
        symbol,
        rank,
        price,
        priceBtc,
        volume,
        marketCap,
        availableSupply,
        totalSupply,
        priceChange1h,
        priceChange1d,
        priceChange1w,
        websiteUrl,
        twitterUrl,
        exp,
        contractAddress,
        decimals,
      );

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CoinModel &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          icon == other.icon &&
          name == other.name &&
          symbol == other.symbol &&
          rank == other.rank &&
          price == other.price &&
          priceBtc == other.priceBtc &&
          volume == other.volume &&
          marketCap == other.marketCap &&
          availableSupply == other.availableSupply &&
          totalSupply == other.totalSupply &&
          priceChange1h == other.priceChange1h &&
          priceChange1d == other.priceChange1d &&
          priceChange1w == other.priceChange1w &&
          websiteUrl == other.websiteUrl &&
          twitterUrl == other.twitterUrl &&
          listEquals(exp, other.exp) &&
          contractAddress == other.contractAddress &&
          decimals == other.decimals;
}
