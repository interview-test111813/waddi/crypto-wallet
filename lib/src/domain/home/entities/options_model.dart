import 'package:flutter/material.dart';

class HomeHeaderOptionModel {
  final String name;
  final Icon icon;

  HomeHeaderOptionModel({required this.name, required this.icon});

  static List<HomeHeaderOptionModel> options = [
    HomeHeaderOptionModel(
      name: 'send',
      icon: const Icon(
        Icons.send,
        size: 24,
      ),
    ),
    HomeHeaderOptionModel(
      name: 'receive',
      icon: const Icon(
        Icons.call_received,
        size: 24,
      ),
    ),
    HomeHeaderOptionModel(
      name: 'buy',
      icon: const Icon(
        Icons.wallet,
        size: 24,
      ),
    ),
    HomeHeaderOptionModel(
      name: 'swap',
      icon: const Icon(
        Icons.swap_horiz,
        size: 24,
      ),
    )
  ];
}
