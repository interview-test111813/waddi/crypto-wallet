import 'package:waddicryptowallet/src/domain/home/entities/coin_model.dart';

mixin CoinsMixin {
  /// Actualiza la lista de monedas [assets] con la información de [newAssets].
  ///
  /// Devuelve una nueva lista actualizada con los cambios.
  List<CoinModel> updateCoinListInformation(
      List<CoinModel> assets, List<CoinModel> newAssets) {
    List<CoinModel> updatedList = List.from(assets);

    for (var newItem in newAssets) {
      int existingIndex = assets.indexWhere((item) => item.id == newItem.id);
      if (existingIndex != -1) {
        updatedList[existingIndex] = newItem;
      } else {
        updatedList.add(newItem);
      }
    }

    return updatedList;
  }

  /// Ordena la lista de monedas [assets] por precio en orden descendente.
  ///
  /// Devuelve la lista de monedas ordenada por precio.
  List<CoinModel> sortByPrice(List<CoinModel> assets) {
    assets.sort((a, b) => b.price.compareTo(a.price));
    return assets;
  }

  /// Ordena la lista de monedas [assets] alfabéticamente por nombre.
  ///
  /// Devuelve la lista de monedas ordenada por nombre.
  List<CoinModel> sortByName(List<CoinModel> assets) {
    assets.sort((a, b) => a.name.compareTo(b.name));
    return assets;
  }
}
