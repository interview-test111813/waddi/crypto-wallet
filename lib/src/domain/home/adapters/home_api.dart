import 'package:waddicryptowallet/src/domain/home/entities/coin_model.dart';

abstract class HomeApi {
  Future<List<CoinModel>> getCryptoAssets();
}
