abstract class HomeLocalApi {
  List<String> getAssetsFavorites();
  void saveAssetsFavorites(List<String> assets);
}
