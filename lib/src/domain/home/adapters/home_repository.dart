import 'package:dartz/dartz.dart';
import 'package:waddicryptowallet/src/domain/common/base/base_failure.dart';
import 'package:waddicryptowallet/src/domain/home/entities/coin_model.dart';

abstract class CryptoAssetsRepository {
  Future<Either<List<CoinModel>, Failure>> getAssets();
}
