import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:waddicryptowallet/generated/l10n.dart';
import 'package:waddicryptowallet/routes/routes.dart';
import 'package:waddicryptowallet/src/presentation/home/bloc/home_bloc.dart';
import 'package:waddicryptowallet/src/utils/common.dart';
import './injection_container.dart' as di;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

  await di.init();

  runApp(const WaddiCryptoWalletApp());
}

class WaddiCryptoWalletApp extends StatelessWidget {
  const WaddiCryptoWalletApp({super.key});

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: di.sl.allReady(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return LayoutBuilder(
              builder: (context, constraints) =>
                  OrientationBuilder(builder: (context, orientation) {
                SizeConfig().init(constraints, orientation);

                return MultiBlocProvider(
                  providers: [
                    BlocProvider<HomeBloc>(create: (_) => di.sl.call()),
                  ],
                  child: GetMaterialApp(
                    debugShowCheckedModeBanner: false,
                    title: 'Waddi Wallet',
                    theme: ThemeData(
                      primarySwatch: Colors.blue,
                    ),
                    localizationsDelegates: const [
                      GlobalMaterialLocalizations.delegate,
                      GlobalWidgetsLocalizations.delegate,
                      GlobalCupertinoLocalizations.delegate,
                      S.delegate
                    ],
                    supportedLocales: S.delegate.supportedLocales,
                    getPages: GlobalRouter.routes,
                    initialRoute: GlobalRouter.routeSplash,
                  ),
                );
              }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }
}
