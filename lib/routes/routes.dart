import 'package:get/get.dart';
import 'package:waddicryptowallet/src/presentation/home/presentation/views/home_page.dart';
import 'package:waddicryptowallet/src/presentation/splash/presentation/views/splash_page.dart';

class GlobalRouter {
  static const routeSplash = "/splash";
  static const routeRoot = "/home";
  static const routeSettings = "/settings";

  static final routes = [
    GetPage(
      name: routeSplash,
      page: () => const SplashPage(),
      transition: Transition.fadeIn,
    ),
    GetPage(
      name: routeRoot,
      page: () => const HomePage(),
      transition: Transition.fadeIn,
    ),
  ];
}
