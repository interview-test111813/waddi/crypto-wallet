Crypto Wallet App

This is a cryptocurrency wallet application that allows users to view a list of available assets (coins), sort them, and mark them as favorites. The app includes features like filtering assets, refreshing the asset list periodically, sorting assets by name or price, grouping favorite assets, and additional convenient features.

Features

Asset List: Display the assets in a grid or list format.
Automatic Refresh: Refresh the asset list every 60 seconds.
Sorting: Provide options to sort assets by name or price.
Favorite Selection: Allow users to mark assets as favorites.
Favorite Asset Grouping: Group and display favorite assets separately.
Dynamic Sorting with Automatic Refresh: Allow users to sort assets by name or price, and automatically refresh the list every 60 seconds.
Additional Convenient Features: Implement any additional features that enhance the user experience.
Technologies and Architecture

Flutter: The app will be developed using the Flutter framework.
API: Utilize the following API for retrieving asset data: Cryptocurrency API.
Dependency Injection: Implement dependency injection for better code organization and testability.
Architecture: Choose an appropriate architecture pattern such as MVVM, MVP, MVI, VIPER, etc. for a structured and maintainable codebase.
Unittest: Include unit tests to ensure the correctness of critical functionalities.
Installation and Setup

To run the Crypto Wallet App locally, follow these steps:

Clone the repository from GitHub/Bitbucket/GitLab.
Install Flutter and set up the development environment according to the official Flutter documentation.
Open the project in your preferred IDE.
Install the project dependencies using a package manager like pub or Flutter's built-in package manager.
Configure the API endpoint in the app to fetch asset data from the provided API.
Build and run the app on your target device or emulator.
Folder Structure

The app's folder structure is organized as follows:

markdown
Copy code
- lib
    - data: Contains data-related classes and repositories.
    - models: Includes the models representing assets and other data structures.
    - services: Handles API calls and other external services.
    - ui: Contains all the user interface components, views, and widgets.
    - utils: Includes utility classes and helper functions.
    - main.dart: Entry point of the application.
- test: Contains only one unit basic tests.
- README.md: Project documentation and instructions.
  Contributions

Contributions to the Crypto Wallet App are welcome. If you have any ideas for new features, improvements, or bug fixes, please submit a pull request to the repository. Be sure to follow the project's coding style, guidelines, and include appropriate unit tests for any new functionality.

License

The Crypto Wallet App is open source and available under the MIT License. Feel free to use, modify, and distribute the code as per the terms of the license.

Acknowledgements

This app was created as part of a development exercise and utilizes the provided Cryptocurrency API for retrieving asset data. Special thanks to the API creators and maintainers for their work.



